# /packages/intranet-trans-memoq/tcl/intranet-trans-memoq-procs.tcl


ad_library {
   MemoQ server applications

	@author malte.sussdorff@cognovis.de
}

package require http
::http::register https 443 ::tls::socket

ad_proc -public im_package_trans_memoq_id {} {
    Returns the package id of the intranet-trans-memoq package
} {
    return [util_memoize im_package_trans_memoq_id_helper]
}

ad_proc -private im_package_trans_memoq_id_helper {} {
    return [db_string im_package_core_id {
	select package_id from apm_packages
	where package_key = 'intranet-trans-memoq'
    } -default 0]
}

ad_proc -public im_trans_memoq_configured_p {
} {
    Return 1 if trados server is correctly configured and reachable
    
    Cached for 10 minutes
} {
    return [util_memoize  [list im_trans_memoq_configured_helper] 600]
}

ad_proc -public im_trans_memoq_configured_helper {
} {
    Return 1 if memoq server is correctly configured and reachable
} {
    set return_value 1

    # Figure out if we can even run the analysis and set the parameters
    set memoq_server [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQServer"]
    set memoq_port [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQPort"]
    if {$memoq_server eq "" || $memoq_port eq ""} {
	ns_log Notice "MemoQ Server not configured, please configure it first in the parameter section"
	set return_value 0
    }
    
    # Check if we can reach the server
    if {$return_value eq 1} {
	if {[catch {::http::geturl ${memoq_server}:${memoq_port}/memoq -type {text/xml;charset=utf-8}} errmsg]} {
	    set return_value 0
	    ns_log Error "MemoQ Server configured but unreachable. $errmsg"
	}
    }
    return $return_value
}

ad_proc -public im_trans_memoq_api_configured_p {
} {
    Return 1 if trados server is correctly configured and reachable
    
    Cached for 10 minutes
} {
    return [util_memoize  [list im_trans_memoq_api_configured_helper] 600]
}

ad_proc -public im_trans_memoq_api_configured_helper {
} {
    Return 1 if memoq server is correctly configured and reachable
} {
    set return_value 1

    # Figure out if we can even run the analysis and set the parameters
    set memoq_server [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQAPIServer"]
    set memoq_port [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQAPIPort"]
    set api_key [parameter::get -package_id [im_package_trans_memoq_id] -parameter "APIKey"]
    if {$memoq_server eq "" || $memoq_port eq "" || $api_key eq ""} {
	ns_log Notice "MemoQ Server not configured, please configure it first in the parameter section"
	set return_value 0
    }
    
    # Check if we can reach the server
    if {$return_value eq 1} {
	if {[catch {::http::geturl ${memoq_server}:${memoq_port}/memoqservices/tm -type {text/xml;charset=utf-8}} errmsg]} {
	    set return_value 0
	    ns_log Error "MemoQ Server configured but unreachable. $errmsg"
	}
    }
    return $return_value
}

ad_proc -public im_trans_memoq_call {
	-service_name
	-operation
	-m_Xml
	-debug:boolean
} {
	Call the memoq server with the soapXML. Takes care of API Keys as well
	
	@param service_name Name of the Service to use, e.g. TMService
	@param operation Operation to call. Will also be used for SOAP calls, e.g.GetTMInfo
	@param m_Xml Body for the MemoQ service
	
	@return WSDL from MemoQ
} {


	# ---------------------------------------------------------------
	# URL Setting
	# ---------------------------------------------------------------
	switch $service_name {
		TMService {
			set base_url "/memoqservices/tm"
		}
		TBService {
			set base_url "/memoqservices/tb"
		}
		FileManagerService {
			set base_url "/memoqservices/filemanager"
		}
		LiveDocsService {
			set base_url "/memoqservices/livedocs"
		}
		ResourceService {
			set base_url "/memoqservices/resource"
		}
		SecurityService {
			set base_url "/memoqservices/security"
		}
		ServerProjectService {
			set base_url "/memoqservices/serverproject"
		}
	}

	set memoq_server [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQAPIServer"]
	set memoq_port [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQAPIPort"]
	set api_key [parameter::get -package_id [im_package_trans_memoq_id] -parameter "APIKey"]
	
	if {![im_trans_memoq_configured_p]} {
	    return 0
	} else {
		set soapXml [new_CkXml]
		CkXml_put_Tag $soapXml "s:Envelope"
		
		set success [CkXml_AddAttribute $soapXml "xmlns:s" "http://schemas.xmlsoap.org/soap/envelope/"]
		set success [CkXml_AddAttribute $soapXml "xmlns:m" "http://kilgray.com/memoqservices/2007"]

#		set success [CkXml_AddAttribute $soapXml "xmlns:SOAP-ENC" "http://schemas.xmlsoap.org/soap/encoding"]
#		set success [CkXml_AddAttribute $soapXml "xmlns:xsi" "http://www.w3.org/2001/XMLSchema-instance"]
#		set success [CkXml_AddAttribute $soapXml "xmlns:xs" "http://www.w3.org/2001/XMLSchema"]
#		set success [CkXml_AddAttribute $soapXml "xmlns:soap" "http://schemas.xmlsoap.org/soap/envelope/"]
#		set success [CkXml_AddAttribute $soapXml "xmlns" "http://kilgray.com/memoqservices/2007"]
#		set success [CkXml_AddAttribute $soapXml "xmlns:w" "http://schemas.xmlsoap.org/wsdl/"]

		CkXml_NewChild2 $soapXml "s:Header|ApiKey" "$api_key"
				
		CkXml_NewChild2 $soapXml "s:Body" ""

		set success [CkXml_GetChild2 $soapXml 1]
		CkXml_AddChildTree $soapXml $m_Xml
		CkXml_GetRoot2 $soapXml
		
		set query [CkXml_getXml $soapXml]
		
		if {$debug_p} {
			ns_log Notice "Query:: $query"
			ns_log Notice "Calling:: ${memoq_server}:${memoq_port}${base_url}/$service_name"
		} else {
			ns_log Notice "im_trans_memoq_call:: $service_name :: $operation"
		}

		set token [::http::geturl ${memoq_server}:${memoq_port}${base_url}/$service_name -query $query -type {text/xml;charset=utf-8} -headers [list SOAPAction [list "http://kilgray.com/memoqservices/2007/I${service_name}/$operation"]]]

		set response [::http::data $token]

	    if {$debug_p} {
		set responseXml [new_CkXml]
		set success [CkXml_LoadXml $responseXml $response]
		
		ns_log Notice "REPONSE:: [CkXml_getXml $responseXml]"
		delete_CkXml $responseXml
	    }
	    delete_CkXml $soapXml
	    delete_CkXml $m_Xml
	    
	    return $response
	}
}

ad_proc -public im_trans_memoq_import_tm_file {
	-tmGuid
	-tmx_file
} {
	Imports the file into the MemoQ TM specified by tmGuid
	
	@param tmGuid GUID for the MemoQ TM
	@param file file path to the tmx file
} {
	
	# Load the file
	set tmxIn [new_CkFileAccess]
	
	set success [CkFileAccess_OpenForRead $tmxIn $tmx_file]
	set chunkSize 500000
	set numChunks [CkFileAccess_GetNumBlocks $tmxIn $chunkSize]

	# Get the session_id
	set session_id [intranet_memoq::TMService::BeginChunkedTMXImport -tmGuid $tmGuid]
	
	set binData [new_CkBinData]
	set i 0
	while {[expr $i < $numChunks]} {
		set i [expr $i + 1]

		#  Read the next chunk from the file.
		CkBinData_Clear $binData ; # First through out the old chunk
		CkFileAccess_FileReadBd $tmxIn $chunkSize $binData
		
		intranet_memoq::TMService::AddNextTMXChunk -sessionId $session_id -tmxData [CkBinData_getEncoded $binData Base64]
	}
	
	CkFileAccess_FileClose $tmxIn

	# End the import session
	return [intranet_memoq::TMService::EndChunkedTMXImport -sessionId $session_id]

}

ad_proc -public im_trans_memoq_export_tm_file {
	-tmGuid
	-tmx_file
} {
	Exports the file into the MemoQ TM specified by tmGuid

	@param tmGuid GUID for the MemoQ TM
	@param file file path to the tmx file
} {


	# Get the session_id
	set session_id [intranet_memoq::TMService::BeginChunkedTMXExport -tmGuid $tmGuid]

	set binData [new_CkBinData]
	set chunk "1"
	set full_data ""
	while {$chunk ne ""} {
		set chunk [intranet_memoq::TMService::GetNextTMXChunk -sessionId $session_id]
		append full_data $chunk
	}

	# Save the content to a file.
    set file [open $tmx_file w]
    fconfigure $file -encoding "utf-8"
    puts $file [string convertto utf-8 [::base64::decode $full_data]]
    flush $file
    close $file

	# End the import session
	intranet_memoq::TMService::EndChunkedTMXExport -sessionId $session_id
}

ad_proc -public memoq_project_domain {
	-project_id
} {
	Get the value for domain for a project, based on the Parameter MemoQDomainAttribute which defines the dynfield attribute

	@param project_id ID of the project you want to get the domain for

	@return dynfield attribute value for the attribute
} {
	set attribute_name [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQDomainAttribute"]
	if {$attribute_name ne ""} {
		db_1row attribute_info "select a.table_name, coalesce(a.column_name,a.attribute_name) as column_name, aw.deref_plpgsql_function from acs_attributes a, im_dynfield_attributes aa, im_dynfield_widgets aw where aa.widget_name = aw.widget_name and aa.acs_attribute_id = a.attribute_id and object_type = 'im_project' and a.attribute_name = :attribute_name"

		set value [db_string get_value "select ${deref_plpgsql_function}($column_name) from $table_name where project_id = :project_id"]
	} else {
		set value ""
	}
	return $value
}

ad_proc -public im_trans_memoq_resources_component {
	-company_id
	{-user_id ""}
	{-return_url ""}
} {
	Display the Resources of a company
} {
	set params [list  [list base_url "/intranet-trans-memoq/"]  [list company_id $company_id] [list return_url [im_biz_object_url $company_id]]]

	if {[exists_and_not_null params]} {
		set result [ad_parse_template -params $params "/packages/intranet-trans-memoq/lib/memoq-resources"]
	} else {
		set result ""
	}
}
#{date {date_format "%Y-%m-%d %H:%M:%S"}}
ad_proc -public is_valid_date_for_memoq {
	-date
	{-date_format "%Y-%m-%d %H:%M:%S"}
} {
    date validation for MemoQ
} {
    set result 0
    set date [regsub "T" $date " "]
 
    catch {
        set time [string equal [clock format [clock scan $date  -format $date_format] -format $date_format] $date]
        if {$time == 1} {
            set result 1
        }
    }
    return $result
}


ad_proc -public im_trans_memoq_create_project {
	-project_id
} {
	Create the Project in MemoQ and store the result in ]project-open[
} {


    db_1row project_info "select project_name, project_nr, company_path, description, to_char(end_date,'YYYY-MM-DD') || 'T' || to_char(end_date,'HH24:MI:SS') as deadline, source_language_id, im_category_from_id(subject_area_id) as subject, c.company_id from im_projects p, im_companies c where p.company_id = c.company_id and project_id = :project_id"
    
    set source_language [im_category_from_id -translate_p 0 $source_language_id]
    set target_languages [im_target_languages $project_id]
    set target_language_ids [im_target_language_ids $project_id]
    set domain [memoq_project_domain -project_id $project_id]
    set languages_duplicated_p 0
    if {[lsearch $target_language_ids $source_language_id] > -1} {
	set languages_duplicated_p 1
    }
    # Check if date is valid for MemoQ + check if 'source language' is not duplicated in 'target langauges' list
    if {[is_valid_date_for_memoq -date $deadline] == 1 && $languages_duplicated_p ne 1} {
	array set create_project [intranet_memoq::ServerProjectService::CreateProject -Name "$project_name" \
				      -Description $description \
				      -SourceLanguageCode $source_language \
				      -TargetLanguageCodes $target_languages \
				      -Project $project_nr \
				      -Subject $subject \
				      -Client $company_path \
				      -Deadline $deadline \
				      -Domain $domain]
	if {$create_project(error_p) eq 0} {
	    # Update the Project with the Guid
	    set memoq_guid $create_project(memoq_guid)
	    db_dml update_guid "update im_projects set memoq_guid = :memoq_guid where project_id = :project_id"

	    # Find the TMs for the company and ammend them
	    set tmGuids [db_list project_guids "select guid from im_trans_memoq_resources
			    where source_language_id = :source_language_id
			    and target_language_id in ([template::util::tcl_to_sql_list $target_language_ids])
			    and type = 'tmx'
			    and company_id = :company_id
		    "]
	    if {[llength $tmGuids] >0} {
		intranet_memoq::ServerProjectService::SetProjectTMs2 -serverProjectGuid $memoq_guid -tmGuids $tmGuids
	    }
	} else {
	    set memoq_guid ""
	    set error_msg "MemoQ Project could not be created: $create_project(message)"
	    sencha_errors::add_warning -object_id $project_id -problem $error_msg
	}
    } else {
	set memoq_guid ""
    }
    return $memoq_guid
}

ad_proc -public im_trans_memoq_file_upload_source_files {
	-project_id
} {
    Upload files for the project
} {
    set project_dir [im_filestorage_project_path $project_id]
    set source_folder [im_trans_task_folder -project_id $project_id -folder_type source]
    set source_dir "$project_dir/${source_folder}"
    if {[catch {glob -directory $source_dir "*"} source_files]} {
	set source_files [list]
    }
    
    set serverProjectGuid [db_string project "select memoq_guid from im_projects where project_id = :project_id" -default ""]
    
    # Try to generate the project in memoq
    if {$serverProjectGuid eq ""} {
	im_trans_memoq_create_project -project_id $project_id
    }

    set source_files_success [list]

    # First unzip all zipped files
    foreach source_file $source_files {
	if {[file extension $source_file] eq ".zip"} {
	    intranet_chilkat::unzip_files -zipfile $source_file -dest_path $source_dir
	    file delete -force $source_file
	}
    }

    if {[catch {glob -directory $source_dir "*"} source_files]} {
	set source_files [list]
    }

    if {$serverProjectGuid ne ""} {
	foreach source_file $source_files {
	    set filename [file tail $source_file]
	    ns_log Notice "file... $source_file [file extension $source_file]"

	    set already_uploaded_p [db_string uploaded "select 1 from im_trans_tasks where memoq_doc_guid is not null and task_filename = :filename and project_id = :project_id limit 1" -default 0]
	    
	    if {$already_uploaded_p} {
		continue
	    }
	    
	    # Start uploading the source file
	    set fileIn [new_CkFileAccess]
	    
	    set success [CkFileAccess_OpenForRead $fileIn $source_file]
	    set chunkSize 500000
	    set numChunks [CkFileAccess_GetNumBlocks $fileIn $chunkSize]
	    
	    # Get the session_id
	    set session_id [intranet_memoq::FileManagerService::BeginChunkedFileUpload -fileName $filename]
	    
	    set binData [new_CkBinData]
	    set i 0
	    set success_p 1
	    while {[expr $i < $numChunks]} {
		set i [expr $i + 1]
		
		#  Read the next chunk from the file.
		CkBinData_Clear $binData ; # First through out the old chunk
		CkFileAccess_FileReadBd $fileIn $chunkSize $binData
		
		if {[intranet_memoq::FileManagerService::AddNextFileChunk \
			 -fileIdAndSessionId $session_id -fileData [CkBinData_getEncoded $binData Base64]]} {
		} else {
		    ns_log Error "Error importing chunk $i for $source_file"
		    set i $numChunks
		}
	    }
	    
	    CkFileAccess_FileClose $fileIn
	    
	    # End the import session
	    if {$success_p} {
		set success_p [intranet_memoq::FileManagerService::EndChunkedFileUpload -fileIdAndSessionId $session_id]
	    } else {
		# We still need to end the file, even if the chunks failed...
		intranet_memoq::FileManagerService::EndChunkedFileUpload -fileIdAndSessionId $session_id
	    }
	    
	    if {$success_p} {
		# Check if we have any tasks for this file
		set task_exists_p [db_string uploaded "select 1 from im_trans_tasks where task_filename = :filename and project_id = :project_id limit 1" -default 0]
		
		if {!$task_exists_p} {
		    set target_language_ids [im_target_language_ids $project_id]
		    set project_type_id [db_string project_type "select project_type_id from im_projects where project_id = :project_id"]
		    set uom_id [im_uom_s_word] ; # MemoQ returns me the S-Words in the analysis
		    set task_units "" ; # No analysis done yet
		    im_task_insert $project_id $filename $filename $task_units $uom_id $project_type_id $target_language_ids
		}
		
		# Link the uploaded file to the project
		db_foreach task {select task_id, target_language_id from im_trans_tasks where task_filename = :filename and project_id = :project_id} {
		    set target_language_code [im_category_from_id -translate_p 0 $target_language_id]
		    set memoq_doc_guids [intranet_memoq::ServerProjectService::ImportTranslationDocument \
					     -serverProjectGuid $serverProjectGuid -fileGuid $session_id \
					     -targetLangCodes $target_language_code]
		    
		    if {[llength $memoq_doc_guids] eq 1} {
			set memoq_doc_guid [lindex $memoq_doc_guids 0]
			db_dml update_doc_guid "update im_trans_tasks set memoq_doc_guid = :memoq_doc_guid where task_id = :task_id"			
		    }
		}
		lappend source_files_success $source_file
	    }
	    
	    # Delete the uploaded file
	    intranet_memoq::FileManagerService::DeleteFile -fileGuid $session_id
	}
    }
    ns_log Notice "uploaded $source_files_success"
    return $source_files_success
}

ad_proc -public im_trans_memoq_project_analysis {
    -project_id
} {
    Import the current statistics for the project directly into the tasks found for the filename.
    
    If more files are found then we have tasks for them, then we need to find the docGUID for them as well. This can happen if a file was added to memoq, but not through ]project-open[
} {
    
    set serverProjectGuid [db_string project "select memoq_guid from im_projects where project_id = :project_id" -default ""]
    
    if {$serverProjectGuid eq ""} { 
	set serverProjectGuid [im_trans_memoq_create_project -project_id $project_id]
    }
    
    if {$serverProjectGuid eq ""} { 
	set error_msg "MemoQ Project could not be created: You can't analyse for a project where we don't have a serverProjectGuid for" 
    }
    
    set return_lol [intranet_memoq::ServerProjectService::GetStatisticsOnProject -serverProjectGuid $serverProjectGuid]

    foreach line $return_lol {
	template::util::list_of_lists_to_array $line statistics
	template::util::array_to_vars statistics
	if {[info exists file_list($target_language_code)]} {
	    if {[lsearch $file_list($target_language_code) $filename] <0} {
		lappend file_list($target_language_code) $filename
	    } else {
		# We already have this file, ignore it and continue
		continue
	    }
	    
	}
	
	set target_language_id [im_id_from_category $target_language_code "Intranet Translation Language"]
	
	# Find the task_id and create it if not found
	set task_id [db_string task "select task_id from im_trans_tasks where task_filename = :filename and project_id = :project_id and target_language_id = :target_language_id" -default ""]
	
	db_1row project_type "select project_type_id, company_id as customer_id from im_projects where project_id = :project_id"
	
	
	if {$task_id eq ""} {
	    # Create the task
	    set task_id [im_task_insert $project_id $filename $filename "" "[im_uom_s_word]" $project_type_id $target_language_id]
	    
	}
	
	if {$task_id ne ""} {
	    # ---------------------------------------------------------------
	    # Update the statistics now
	    # ---------------------------------------------------------------
	    set task_units [im_trans_trados_matrix_calculate [im_company_freelance] $px_words $prep_words $p100_words $p95_words $p85_words $p75_words $p50_words $p0_words]
	    set billable_units [im_trans_trados_matrix_calculate $customer_id $px_words $prep_words $p100_words $p95_words $p85_words $p75_words $p50_words $p0_words]
	    set billable_units_interco $billable_units
	    set interco_p [parameter::get_from_package_key -package_key "intranet-translation" -parameter "EnableInterCompanyInvoicingP" -default 0]
	    
	    if {$interco_p} {
		set interco_company_id [db_string get_interco_company "select interco_company_id from im_projects where project_id=$project_id" -default ""]
		if {"" == $interco_company_id} {
		    set interco_company_id $customer_id
		}
		set billable_units_interco [im_trans_trados_matrix_calculate $interco_company_id $px_words $prep_words $p100_words $p95_words $p85_words $p75_words $p50_words $p0_words]
	    }
	    
	    db_dml update_task "
		UPDATE im_trans_tasks SET
		tm_integration_type_id = [im_trans_tm_integration_type_external],
		task_units = :task_units,
		billable_units = :billable_units,
		billable_units_interco = :billable_units_interco,
		match_x = :px_words,
		match_rep = :prep_words,
		match100 = :p100_words,
		match95 = :p95_words,
		match85 = :p85_words,
		match75 = :p75_words,
		match50 = :p50_words,
		match0 = :p0_words
		WHERE
		task_id = :task_id
	    "
	    im_audit -object_type "im_trans_task" -object_id $task_id -action "after_update" -status_id 340 -type_id $project_type_id
	}
    }
}

