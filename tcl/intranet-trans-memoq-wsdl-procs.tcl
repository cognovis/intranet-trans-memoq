# /packages/intranet-trans-memoq/tcl/intranet-trans-memoq-procs.tcl

ad_library {
   MemoQ server applications

	@author malte.sussdorff@cognovis.de
}
namespace eval intranet_memoq::ResourceService {}
namespace eval intranet_memoq::SecurityService {}


ad_proc -public intranet_memoq::ResourceService::ListResources {
	{-resourceType ""}
} {
	Returns the list of the light resources available on the server.
	
	@param resourceType The type of the resource to be listed.
	@return An array of LightResourceInfo (or derived, depending the type of the resource) objects is returned. To find out exactly which derived class is returned here for a resource type, see the "Described by class" section of the documentation of the appropriate "ResourceType" enum member (e.g.ResourceInfoWithLang for SegRules).
} {
	set operation "ListResources"

	set listXml [new_CkXml]
	CkXml_put_Tag $listXml "m:$operation"


	if {$resourceType ne ""} {
		CkXml_NewChild $listXml m:resourceType $resourceType
	}

	CkXml_GetRoot2 $listXml

	set response [im_trans_memoq_call -service_name ResourceService -operation $operation -m_Xml $listXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]

	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]

	set success [CkXml_FirstChild2 $responseXml]
	while {[expr $success == 1]} {
		ns_log Notice "[CkXml_tag $responseXml] : [CkXml_content $responseXml]"
		set success [CkXml_NextSibling2 $responseXml]
	}

	delete_CkXml $responseXml
}

ad_proc -public intranet_memoq::SecurityService::ListUsers {
	
} {
	Returns the list of MemoQ Server Users
} {
	set operation "ListUsers"
	
	set listXml [new_CkXml]
	CkXml_put_Tag $listXml "m:$operation"
	CkXml_GetRoot2 $listXml
	
	set response [im_trans_memoq_call -service_name SecurityService -operation $operation -m_Xml $listXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]
		
	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
	
	set success [CkXml_FirstChild2 $responseXml]
	while {[expr $success == 1]} {
		ns_log Notice "[CkXml_tag $responseXml] : [CkXml_content $responseXml]"
		set success [CkXml_NextSibling2 $responseXml]
	}
	
	delete_CkXml $responseXml
}

ad_proc -public intranet_memoq::ResultListOfLists {
	-responseXml
	-Tag
} {
	Returns a list of key value list of lists
} {
	set num_response [CkXml_NumChildrenHavingTag $responseXml $Tag]
	
	set response_list [list]
	for {set i 0} {$i <= [expr $num_response - 1]} {incr i} {
	
		set response [CkXml_GetChild $responseXml $i]
		set info_list [list]
		for {set j 0} {$j <= [expr [CkXml_get_NumChildren $response] - 1]} {incr j} {
			#  Navigate to the Nth child.
			set success [CkXml_GetChild2 $response $j]
			set key [CkXml_tag $response]
			set value [CkXml_content $response]
			#  Navigate back up to the parent:
			set success [CkXml_GetParent2 $response]
			lappend info_list [list $key $value]
			ns_log Notice "$Tag ::: $key $value"
		}
	
		# TODO: Change it so you get the project_ids etc.
		lappend response_list $info_list
		delete_CkXml $response
		set success [CkXml_NextSibling2 $responseXml]
	}
	
	return $response_list

}