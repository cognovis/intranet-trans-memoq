# /packages/intranet-trans-memoq/tcl/intranet-trans-memoq-procs.tcl

ad_library {
 MemoQ server applications

	@author malte.sussdorff@cognovis.de
}
namespace eval intranet_memoq::ServerProjectService {}


ad_proc -public intranet_memoq::ServerProjectService::ListProjects {
	{-SourceLanguageCode ""}
	{-TargetLanguageCode ""}
	{-Domain ""}
	{-Subject ""}
	{-Client ""}
	{-Project ""}
	{-TimeClosed ""}
} {
	Returns a list of Projects

	@param SourceLanguageCode This parameter can be used to filter the list based on the SOURCE language of the server project. The three+(two) letter langusage code of the language is to be provided, such as fre, eng, eng-US. If a three letter /// language code is provided all three+two letter matches are returned (e.g. /// if eng is specified then server projects with source language eng-US, eng-gb, ... and eng are returned. If this parameter is null, no source language filtering is applied.
	@param TargetLanguageCode Filter the list based on the Target language
	@param Domain The domain attribute of the project. If null, no filtering is applied.
	@param Subject The subject attribute of the project. If null, no filtering is applied.
	@param Client The client attribute of the project. If null, no filtering is applied.
	@param Project The project attribute of the project. If null, no filtering is applied.
	@param TimeClosed The TimeClosed attribute of the project. Replaces former IsArchived. Only projects that were closed after (or at the same time) TimeClosed are /// returned (including unclosed projects!). Recommended usage:
- Set it to a distant future value (equal or greater than 1/1/2500) to list unclosed projects only. This is the tipical way of listing server projects. Set to a datetime value in the past to include projects closed (after the specified time). If set to a small datetime value (e.g 1/1/1900) all projects are returned. If you need the list of projects not including unclosed ones, then remove unclosed projects from the result set in your system (based on the TimeClosed attribute of the returned ServerProjectInfo objects. List server projects including closed ones only if you have a reason to do so, as the returned result set may be significantly larger in this case.

	@return List of - ListOfLists with Key Value pairs for each found project
} {
	set operation "ListProjects"

	set listXml [new_CkXml]
	CkXml_put_Tag $listXml "m:$operation"

	# ---------------------------------------------------------------
	# Allow the adding of filters
	# ---------------------------------------------------------------
	set filter_p 0
	set actual_filters [list]
	foreach filter [list SourceLanguageCode TargetLanguageCode Domain Subject Client Project TimeClosed] {
		if {[set $filter] ne ""} {
			set filter_p 1
			lappend actual_filters $filter
		}
	}
	if {$filter_p} {
		set childNode [CkXml_NewChild $listXml "m:filter" ""]

		foreach filter $actual_filters {
			CkXml_NewChild $childNode m:$filter [set $filter]
		}
	}

	CkXml_GetRoot2 $listXml

	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $listXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]

	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
	
	set projects_list [intranet_memoq::ResultListOfLists -responseXml $responseXml -Tag "ServerProjectInfo"]

	delete_CkXml $responseXml
	return $projects_list
}

ad_proc -public intranet_memoq::ServerProjectService::CreateProject {
	-Name
	{-Description ""}
	-SourceLanguageCode
	-TargetLanguageCodes
	-Deadline
	{-Domain ""}
	{-Subject ""}
	{-Client ""}
	{-Project ""}
	{-CreatorUser "00000000-0000-0000-0001-000000000001"}
	{-EnableCommunication "false"}
	{-CreateOfflineTMTBCopies "false"}
	{-EnableSplitJoin "true"}
	{-RecordVersionHistory "false"}
	{-PreventDeliveryOnQAError "false"}
	{-AllowPackageCreation "false"}
	{-AllowOverlappingWorkflow "true"}
	{-DownloadSkeleton "true"}
	{-DownloadPreview "true"}
	{-PackageResourceHandling ""}
	{-CustomMetas ""}
	{-StrictSubLangMatching "false"}
	{-PackageDeliveryOptions ""}
	{-CallbackWebServiceUrl ""}
	{-ArrayName "CreationInfo"}
} {
	Creates a new Project. The user identified by the CreatorUser member of the spInfo parameter is automatically added to the project with the Project Manager role.

	@param Name The name of the Project. Can not be null or empty. The name can not contain characters that are invalid in file names.
	@param Description The description of the project.
	@param SourceLanguageCode The three+(two) letter code of the source language of the server project. (such as fre, eng, eng-US).
	@param TargetLanguageCodes List of the three+(two) letter codes of the target languages of the server project (such as fre, eng, eng-US). Can not be null and the items have to represent valid language codes.
	@param Deadline The deadline of the project in datetime format "2500-01-01T00:00:00"
	@param Domain The domain of the project.
	@param EnableSplitJoin Enables the users to split and join segments.
	@param CreateOfflineTMTBCopies When the project is checked out by a user, she will automatically get an offine copy of the Term bases and Translation memories (as opposed to having them used as remote resources).
	@param Subject The subject of the project. Typically the category for the subject_area_id
	@param Client The client of the project. Typically company_path
	@param Project The project attribute of the project. Typically project_nr
	@param CreatorUser The guid of the user creating the project. This user is automatically assigned to the project with ProjectManager role right after project creation. This ensures the there is a valid user assigned that has the permission to manipulate the projet from the MemoQ Client. This has to be the guid of a valid, existing user.
	@param EnableCommunication If true, online communication (chat) is enabled for the project. No forum will be created/assigner to the project even if true
	@param RecordVersionHistory True if document versioning is enabled for the project.
	@param PreventDeliveryOnQAError Indicates whether QA errors should prevent document deliver or not.
	@param AllowPackageCreation Indicates whether the package creation is allowed or not.
	@param AllowOverlappingWorkflow Indicates whether overlapping workflow phases are allowed or not. It should be false if the AllowPackageCreation property is true.
	@param DownloadSkeleton Includes the skeleton files (required for exporting documents) when a user checks out the project or downloads the project as a package. Without the files export is not possible. Project managers always recieve these files. Setting this flag has the same effect as setting DownloadSkeleton. Either flag is true, the options is treated as enabled.
	@param DownloadPreview  Creates preview when documents are imported. Includes the preview of the documents when a user checks out the project or downloads the project as a package. Without the preview information no preview is available while translating. Setting this flag has the same effect as setting DownloadPreview. Either flag is true, the options is treated as enabled.
	@param PackageResourceHandling Specifies how the TMs, TBs and LiveDocs corpora are included in the packages.
	@param CustomMetas The default values of custom fields that server saves to the primary translation memory when you confirm segments during translation. Each line has three or more columns, separated by tabs: metaName metaType value [value2...valueN]. Possible metaType values: FreeText, Number,DateTime,PickListSingle,PickListMultiple
	@param StrictSublangMatching Whether sublanguages are treated as separate languages during TB lookup.
	@param PackageDeliveryOptions The package delivery options. Null value means the default options. The value of this member is ignored if the package creation is not allowed in the project.
	@param CallbackWebServiceUrl The callback url of an external web service that memoQ Server calls to notify of certain actions. The web service must meet the expected contract. Setting this turns on notification automatically.
	
	@return If successful, return \[list 1 Guid\], otherwise return 0 and error
} {
	
	set operation "CreateProject"
	
	set newProjectXml [new_CkXml]
	CkXml_put_Tag $newProjectXml "m:$operation"

	ns_log Notice "DENUGING MY VAR NAME IS $ArrayName"
	
	# ---------------------------------------------------------------
	# Deal with all the switches
	# ---------------------------------------------------------------
	# NOTE: They need to be in the order MemoQ expects them to be.
	set parameters [list AllowOverlappingWorkflow AllowPackageCreation Client CreatorUser CustomMetas Deadline Description Domain DownloadPreview DownloadSkeleton EnableCommunication Name PackageDeliveryOptions PreventDeliveryOnQAError Project RecordVersionHistory SourceLanguageCode StrictSubLangMatching Subject TargetLanguageCodes]

	set childNode [CkXml_NewChild $newProjectXml "m:spInfo" ""]
	
	ns_log Notice "Deadline $Deadline"
	foreach parameter $parameters {
		switch $parameter {
			TargetLanguageCodes {
				# Set the array
				set targetLangNode [CkXml_NewChild $childNode "m:$parameter" ""]
				CkXml_AddAttribute $targetLangNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
				foreach target_lang_code $TargetLanguageCodes {
					CkXml_NewChild $targetLangNode "a:string" $target_lang_code
				}
				CkXml_GetRoot2 $targetLangNode
			}
			PackageDeliveryOptions - PackageResourceHandling {
				set PackageNode [CkXml_NewChild $childNode "m:$parameter" ""]
				CkXml_AddAttribute $PackageNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
				CkXml_GetRoot2 $PackageNode
			}
			default {
				CkXml_NewChild $childNode m:$parameter [set $parameter]
			}
		}
	}
	
	CkXml_GetRoot2 $newProjectXml

	set response ""
    catch {
    	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $newProjectXml]
    }
    
    if {$response eq ""} {
    	set CreationInfo(error_p) 1
    	set CreationInfo(message) "MemoQ sever is currently unreachable"
    	return [array get CreationInfo]
    }

	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]

	set success [CkXml_FindChild2 $responseXml "s:Body"]


	if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
	    ns_log Error "[CkXml_getXml $responseXml]"
	    CkXml_FindChild2 $responseXml "faultstring"
	    
	    set CreationInfo(error_p) 1
	    set CreationInfo(message) "[CkXml_content $responseXml]"
	    set CreationInfo(memoq_guid) ""

	    return [array get CreationInfo]
	    
	} else {
	    set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	    set success [CkXml_FindChild2 $responseXml "${operation}Result"]
	    set project_guid [CkXml_content $responseXml]

	    set CreationInfo(error_p) 0
	    set CreationInfo(message) "success"
	    set CreationInfo(memoq_guid) "[CkXml_content $responseXml]"
	
	    delete_CkXml $responseXml
	    
	    return [array get CreationInfo]
	}
}

ad_proc -public intranet_memoq::ServerProjectService::UpdateProject {
	-ServerProjectGuid
	{-Description ""}
	{-Deadline ""}
	{-Domain ""}
	{-Subject ""}
	{-Client ""}
	{-Project ""}
	{-CallbackWebServiceUrl ""}
	{-CustomMetas ""}
} {
	Updates an existing project
	
	@param ServerProjectGuid The guid of the server project.
	@param Description The description of the project.
	@param Deadline The deadline of the project in datetime format "2500-01-01T00:00:00"
	@param Domain The domain of the project.
	@param Subject The subject of the project. Typically the category for the subject_area_id
	@param Client The client of the project. Typically company_path
	@param Project The project attribute of the project. Typically project_nr
	@param CustomMetas The default values of custom fields that server saves to the primary translation memory when you confirm segments during translation. Each line has three or more columns, separated by tabs: metaName metaType value [value2...valueN]. Possible metaType values: FreeText, Number,DateTime,PickListSingle,PickListMultiple
	@param CallbackWebServiceUrl The callback url of an external web service that memoQ Server calls to notify of certain actions. The web service must meet the expected contract. Setting this turns on notification automatically.

	@return 1 if successful
} {

	set operation "UpdateProject"

	set updateProjectXml [new_CkXml]
	CkXml_put_Tag $updateProjectXml "m:$operation"

	# ---------------------------------------------------------------
	# Deal with all the switches
	# ---------------------------------------------------------------
	# NOTE: They need to be in the order MemoQ expects them to be.
	set parameters [list CallbackWebServiceUrl Client CustomMetas Deadline Description Domain Project ServerProjectGuid Subject]

	set childNode [CkXml_NewChild $updateProjectXml "m:spInfo" ""]

	foreach parameter $parameters {
		if {[set $parameter] ne ""} {
			switch $parameter {
				TargetLanguageCodes {
					# Set the array
					set targetLangNode [CkXml_NewChild $childNode "m:$parameter" ""]
					CkXml_AddAttribute $targetLangNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
					foreach target_lang_code $TargetLanguageCodes {
						CkXml_NewChild $targetLangNode "a:string" $target_lang_code
					}
					CkXml_GetRoot2 $targetLangNode
				}
				PackageDeliveryOptions - PackageResourceHandling {
					set PackageNode [CkXml_NewChild $childNode "m:$parameter" ""]
					CkXml_AddAttribute $PackageNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
					CkXml_GetRoot2 $PackageNode
				}
				default {
					CkXml_NewChild $childNode m:$parameter [set $parameter]
				}
			}
		}
	}

	CkXml_GetRoot2 $updateProjectXml

	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $updateProjectXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]

	set success [CkXml_FindChild2 $responseXml "s:Body"]

	if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
		ns_log Error "[CkXml_getXml $responseXml]"
		delete_CkXml $responseXml
		return 0
	} else {
		delete_CkXml $responseXml
		return 1
	}
}

ad_proc -public intranet_memoq::ServerProjectService::CreateProjectFromTemplate {
	-TemplateGuid
	{-Name ""}
	{-Description ""}
	-SourceLanguageCode
	-TargetLanguageCodes
	{-Domain ""}
	{-Subject ""}
	{-Client ""}
	-Project
	{-CreatorUser "00000000-0000-0000-0001-000000000001"}
} {
	Creates a new Project Based on a template. The user identified by the CreatorUser member of the spInfo parameter is automatically added to the project with the Project Manager role.

	@param TemplateGuid The guid of the template to use. (see listresources)
	@param Name The name of the Project. Can not be null or empty. The name can not contain characters that are invalid in file names.
	@param Description The description of the project.
	@param SourceLanguageCode The three+(two) letter code of the source language of the server project. (such as fre, eng, eng-US).
	@param TargetLanguageCodes List of the three+(two) letter codes of the target languages of the server project (such as fre, eng, eng-US). Can not be null and the items have to represent valid language codes.
	@param Deadline The deadline of the project in datetime format "2500-01-01T00:00:00"
	@param Domain The domain of the project.
	@param Subject The subject of the project. Typically the category for the subject_area_id
	@param Client The client of the project. Typically company_path
	@param Project The project attribute of the project. Typically project_nr
	@param CreatorUser The guid of the user creating the project. This user is automatically assigned to the project with ProjectManager role right after project creation. This ensures the there is a valid user assigned that has the permission to manipulate the projet from the MemoQ Client. This has to be the guid of a valid, existing user.

	@return GUID for the new Project
} {

	set operation "CreateProjectFromTemplate"

	set newProjectXml [new_CkXml]
	CkXml_put_Tag $newProjectXml "m:$operation"

	# ---------------------------------------------------------------
	# Deal with all the switches
	# ---------------------------------------------------------------
	# NOTE: They need to be in the order MemoQ expects them to be.

	set childNode [CkXml_NewChild $newProjectXml "m:createInfo" ""]
	set parameters [list Client CreatorUser Description Domain Name Project ProjectCreationAspects SourceLanguageCode Subject TargetLanguageCodes TemplateGuid]
	foreach parameter $parameters {
		switch $parameter {
			TargetLanguageCodes {
				# Set the array
				set targetLangNode [CkXml_NewChild $childNode "m:$parameter" ""]
				CkXml_AddAttribute $targetLangNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
				foreach target_lang_code $TargetLanguageCodes {
					CkXml_NewChild $targetLangNode "a:string" $target_lang_code
				}
				CkXml_GetRoot2 $targetLangNode
			}
			ProjectCreationAspects {
				set projectCreation [CkXml_NewChild $childNode "m:$parameter" ""]
				CkXml_AddAttribute $projectCreation "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
				CkXml_GetRoot2 $projectCreation

			}
			default {
				CkXml_NewChild $childNode m:$parameter [set $parameter]
			}
		}
	}

	CkXml_GetRoot2 $newProjectXml
	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $newProjectXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]
	
	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]

	set success [CkXml_FirstChild2 $responseXml]
	while {[expr $success == 1]} {
		ns_log Notice "[CkXml_tag $responseXml] : [CkXml_content $responseXml]"
		set success [CkXml_NextSibling2 $responseXml]
	}

	delete_CkXml $responseXml
}


ad_proc -public intranet_memoq::ServerProjectService::GetProject {
	-serverProjectGuid
} {
	Get info about the Project
} {
	set operation "GetProject"
	
	set projectXml [new_CkXml]
	CkXml_put_Tag $projectXml "m:$operation"
	CkXml_NewChild $projectXml "m:spGuid" $serverProjectGuid
	
	CkXml_GetRoot2 $projectXml
	
	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $projectXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]
		
	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
	
	set success [CkXml_FirstChild2 $responseXml]
	while {[expr $success == 1]} {
		ns_log Notice "[CkXml_tag $responseXml] : [CkXml_content $responseXml]"
		set success [CkXml_NextSibling2 $responseXml]
	}
	
	delete_CkXml $responseXml

}

ad_proc -public intranet_memoq::ServerProjectService::GetStatisticsOnProject {
    -serverProjectGuid
    {-targetLangCodes ""}
    {-options ""}
    {-resultFormat "CSV_Trados"}
    {-ShowResultsPerFile "true"}
    {-IncludeLockedRows "false"}
    {-DisableCrossFileRepetition "true"}
} {
    Creates the statistics on the whole server project (includes all documents).
    
    @param serverProjectGuid The guid of the server project.
    @param targetLangCodes The three+(two) letter codes of target languages for which project level statistics is to be created. If null, the statistics is created for all target languages.
    @param options The options of the statistics.
    @param resultFormat The requested result format.
    
    @return The result in the requested format. Important note: the returned StatisticsResultInfo.ResultsForTargetLangs
    does not inlude items for which target language no document exists in the project.
} {
    
    
    set operation "GetStatisticsOnProject"
    
    set statsXml [new_CkXml]
    CkXml_put_Tag $statsXml "m:$operation"
    CkXml_NewChild $statsXml m:serverProjectGuid $serverProjectGuid
    
    if {$targetLangCodes ne ""} {
	# Set the array
	set targetLangNode [CkXml_NewChild $statsXml "m:targetLangCodes" ""]
	CkXml_AddAttribute $targetLangNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
	foreach target_lang_code $targetLangCodes {
	    CkXml_NewChild $targetLangNode "a:string" $target_lang_code
	}
	CkXml_GetRoot2 $targetLangNode
    }
    
    # ---------------------------------------------------------------
    # Options
    # ---------------------------------------------------------------
    set childNode [CkXml_NewChild $statsXml "m:options" ""]
    foreach option [list ShowResultsPerFile IncludeLockedRows DisableCrossFileRepetition] {
	CkXml_NewChild $childNode m:$option [set $option]
    }
    
    # Default options
    CkXml_NewChild $childNode m:Algorithm "Trados"
    foreach option [list Analysis_Homogenity Analysis_ProjectTMs Analyzis_DetailsByTM RepetitionPreferenceOver100 ShowCounts ShowCounts_IncludeTargetCount ShowCounts_IncludeWhitespacesInCharCount ShowCounts_StatusReport] {
	CkXml_NewChild $childNode m:$option "true"
    }
    
    
    CkXml_NewChild $statsXml m:resultFormat $resultFormat
    
    CkXml_GetRoot2 $statsXml
    
    set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $statsXml]
    set responseXml [new_CkXml]
    set success [CkXml_LoadXml $responseXml $response]
    set success [CkXml_FindChild2 $responseXml "s:Body"]
    set success [CkXml_FindChild2 $responseXml "${operation}Response"]
    set success [CkXml_FindChild2 $responseXml "${operation}Result"]
    set success [CkXml_FindChild2 $responseXml "ResultsForTargetLangs"]
    set success [CkXml_FirstChild2 $responseXml]

    set return_lol [list]
    while {[expr $success == 1]} {
	# Go down one level
	set success [CkXml_FindChild2 $responseXml "ResultData"]
	
	set ck_string [new_CkString]
	CkString_append $ck_string "[CkXml_content $responseXml]"
	CkString_base64Decode $ck_string iso-8859-1
	set content	[CkString_getString $ck_string]
	delete_CkString $ck_string
	
	set success [CkXml_NextSibling2 $responseXml]
	set language "[CkXml_content $responseXml]"
	
	ns_log Notice "Analysies for $language resulted in $content"
	if {$resultFormat eq "CSV_Trados"} {
	    set trados_files [split $content "\n"]
	    set trados_files_len [llength $trados_files]
	    set trados_header [lindex $trados_files 1]
	    set separator ";"
	    
	    set trados_headers [split $trados_header $separator]
	    # Distinguish between Trados 3 and Trados 5.5 files
	    #
	    set line2 [lindex $trados_files 2]
	    set line2_len [llength [split $line2 $separator]]
	    
	    if {";" == $separator} {
		switch $line2_len {
		    40 { set trados_version "6.5" }
		    39 { set trados_version "5.5" }
		    38 { set trados_version "5.0" }
		    25 { set trados_version "3" }
		    default { set trados_version "unknown" }
		}
	    } else {
		switch $line2_len {
		    41 { set trados_version "7.0" }
		    40 { set trados_version "6.5" }
		    39 { set trados_version "6.0" }
		    default { set trados_version "unknown" }
		}
	    }
	    
	    set ctr 0
	    
	    # Ignore the header line, but then append all others
	    for {set i 2} {$i < $trados_files_len} {incr i} {
		incr ctr
		set trados_line [lindex $trados_files $i]
		if {0 == [string length $trados_line]} { continue }
		
		# This is the line we return
		set one_line_list [list [list target_language_code $language]]
		
		set trados_fields [split [encoding convertfrom utf-8 $trados_line] $separator]
		lappend one_line_list [list filename [string trim [lindex $trados_fields 0] \"]]
		lappend one_line_list [list tagging_errors [lindex $trados_fields 1]]
		lappend one_line_list [list chars_per_word [lindex $trados_fields 2]]
		lappend one_line_list [list px_segments [lindex $trados_fields 3]]
		lappend one_line_list [list px_words [lindex $trados_fields 4]]
		lappend one_line_list [list px_placeables [lindex $trados_fields 5]]
		
		lappend one_line_list [list prep_segments [lindex $trados_fields 7]]
		lappend one_line_list [list prep_words [lindex $trados_fields 8]]
		lappend one_line_list [list prep_placeables [lindex $trados_fields 9]]
		
		lappend one_line_list [list p100_segments [lindex $trados_fields 11]]
		lappend one_line_list [list p100_words [lindex $trados_fields 12]]
		lappend one_line_list [list p100_placeables [lindex $trados_fields 13]]
		
		lappend one_line_list [list p95_segments  [lindex $trados_fields 15]]
		lappend one_line_list [list p95_words  [lindex $trados_fields 16]]
		lappend one_line_list [list p95_placeables  [lindex $trados_fields 17]]
		
		lappend one_line_list [list p85_segments  [lindex $trados_fields 19]]
		lappend one_line_list [list p85_words  [lindex $trados_fields 20]]
		lappend one_line_list [list p85_placeables  [lindex $trados_fields 21]]
		
		lappend one_line_list [list p75_segments  [lindex $trados_fields 23]]
		lappend one_line_list [list p75_words  [lindex $trados_fields 24]]
		lappend one_line_list [list p75_placeables  [lindex $trados_fields 25]]
		
		lappend one_line_list [list p50_segments  [lindex $trados_fields 27]]
		lappend one_line_list [list p50_words  [lindex $trados_fields 28]]
		lappend one_line_list [list p50_placeables  [lindex $trados_fields 29]]
		
		lappend one_line_list [list p0_segments [lindex $trados_fields 31]]
		lappend one_line_list [list p0_words [lindex $trados_fields 32]]
		lappend one_line_list [list p0_placeables [lindex $trados_fields 33]]
		
		lappend return_lol $one_line_list
	    }
	} else {
	    set lines [split $content "\n"]
	    foreach line $lines {
		ds_comment $line
	    }
	    set return_lol ""
	}
	# Go up and find next sibling
	set status [CkXml_GetParent2 $responseXml]
	set success [CkXml_NextSibling2 $responseXml]
    }
    delete_CkXml $responseXml
    return $return_lol
}

# ---------------------------------------------------------------
# Resource Handling
# ---------------------------------------------------------------

ad_proc -public intranet_memoq::ServerProjectService::ListProjectTMs2 {
	-serverProjectGuid
	{-targetLangCodes ""}
} {
	Returns the current translation memory assignments of the specified project.
	
	@param serverProjectGuid The guid of the server project.
	@param targetLangCodes The target language codes for which the translation memory assignments are to be returned. If null, assignments for all target languages are returned.
	
	@return The current translation memory assignments for the requested target languages.
} {
	set operation "ListProjectTMs2"

	set projectXml [new_CkXml]
	CkXml_put_Tag $projectXml "m:$operation"
	CkXml_NewChild $projectXml "m:serverProjectGuid" $serverProjectGuid

	# Set the array
	if {$targetLangCodes ne ""} {
		set targetLangNode [CkXml_NewChild $projectXml "m:targetLangCodes" ""]
		CkXml_AddAttribute $targetLangNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
		foreach target_lang_code $targetLangCodes {
			CkXml_NewChild $targetLangNode "a:string" $target_lang_code
		}
		CkXml_GetRoot2 $targetLangNode
	}


	CkXml_GetRoot2 $projectXml

	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $projectXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]


	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]

	set response_lol [intranet_memoq::ResultListOfLists -responseXml $responseXml -Tag "ServerProjectTMAssignmentDetails"]
	delete_CkXml $responseXml

	return $response_lol
}


ad_proc -public intranet_memoq::ServerProjectService::SetProjectTMs2 {
	-serverProjectGuid
	-tmGuids
} {
	Sets the new translation memory assignments for the project (for a subset of the target languages of the project). Also sets the primary translation memories for these target languages.
	
	@param serverProjectGuid The guid of the server project.
	@param tmGuids The TMs to assign to it.
} {
	set operation "SetProjectTMs2"

	set projectXml [new_CkXml]
	CkXml_put_Tag $projectXml "m:$operation"
	CkXml_NewChild $projectXml "m:serverProjectGuid" $serverProjectGuid
	
	# Create tmAssignments (ServerProjectTMAssignmentsForTargetLang)
	# Defines the new TM assignments for the project. Each object in this array defines the new assignment and
	# primary TM for one target language. TM assignment for project target languages for which
	# no ServerProjectTMAssignments object is included in the array is not changed.
	
	# tmAssignments is an array
	set tmAssignmentNode [CkXml_NewChild $projectXml "m:tmAssignments" ""]
	
	foreach tmGuid $tmGuids {
		
		# Get the info from the database
		db_1row tminfo "select * from im_trans_memoq_resources where guid = :tmGuid"
		set target_language_code [im_category_from_id -translate_p 0 $target_language_id]
		set tmAssignmentLangNode [CkXml_NewChild $tmAssignmentNode "m:ServerProjectTMAssignmentsForTargetLang" ""]
		CkXml_NewChild $tmAssignmentLangNode m:MasterTMGuid $guid
		CkXml_NewChild $tmAssignmentLangNode m:PrimaryTMGuid $guid
		
		# GUID Array
		set tmGuidNode [CkXml_NewChild $tmAssignmentLangNode "m:TMGuids" ""]
		CkXml_AddAttribute $tmGuidNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
		CkXml_NewChild $tmGuidNode "a:guid" $tmGuid
		
		CkXml_GetRoot2 $tmGuidNode

		# Add the language
		CkXml_NewChild $tmAssignmentLangNode m:TargetLangCode $target_language_code
		CkXml_GetRoot2 $tmAssignmentLangNode

	}
	CkXml_GetRoot2 $tmAssignmentNode

	CkXml_GetRoot2 $projectXml

	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $projectXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]

	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]

	set success [CkXml_FirstChild2 $responseXml]
	while {[expr $success == 1]} {
		ns_log Notice "[CkXml_tag $responseXml] : [CkXml_content $responseXml]"
		set success [CkXml_NextSibling2 $responseXml]
	}

	delete_CkXml $responseXml

}

ad_proc -public intranet_memoq::ServerProjectService::ImportTranslationDocument {
	-serverProjectGuid
	-fileGuid
	{-targetLangCodes ""}
	{-importSettingsXML ""}
} {
	Imports the translation document, that can be of the file formats supported by memoQ.
	
	@param serverProjectGuid The guid of the server project.
	@param fileGuid The guid of the file to be imported. The file has to be uploaded before calling this operation by IFileManagerService.BeginChunkedFileUpload and related operations. This guid is returned by IFileManagerService.BeginChunkedFileUpload.
	@param targetLangCodes The three+(two) letter code of the target languages the document is to be imported into. If null, the document is imported into all target languages of the project. If not null, only target languages that are subset of the target languages of the project can be specified.
	@param importSettingsXML The settings of the import in memoQ import filter XML format. This setting also determines the filter (txt, doc, etc.) to be used. If null, the extension of the file is used to determine the import filter. The format of the file has slightly changed in memoQ version 4.2: it is the new export format of filter configuration resources (you can create one by exporting a memoQ filter configuration resource using memoQ client).
	
	@return The Imported DocGuids as they are returned from MemoQ
} {
	
	set operation "ImportTranslationDocument"
	
	set fileProjectXml [new_CkXml]
	CkXml_put_Tag $fileProjectXml "m:$operation"
	
	CkXml_NewChild $fileProjectXml "m:serverProjectGuid" $serverProjectGuid
	CkXml_NewChild $fileProjectXml "m:fileGuid" $fileGuid
	
	# Set the array
	if {$targetLangCodes ne ""} {
		set targetLangNode [CkXml_NewChild $fileProjectXml "m:targetLangCodes" ""]
		CkXml_AddAttribute $targetLangNode "xmlns:a" "http://schemas.microsoft.com/2003/10/Serialization/Arrays"
		foreach target_lang_code $targetLangCodes {
			CkXml_NewChild $targetLangNode "a:string" $target_lang_code
		}
		CkXml_GetRoot2 $targetLangNode
	}
	
	if {$importSettingsXML ne ""} {
		CkXml_NewChild $fileProjectXml "m:importSettingsXML" $importSettingsXML
	}
	
	CkXml_GetRoot2 $fileProjectXml
	
	set response [im_trans_memoq_call -service_name ServerProjectService -operation $operation -m_Xml $fileProjectXml]
	set responseXml [new_CkXml]

 	set responseXml [new_CkXml]
 	set success [CkXml_LoadXml $responseXml $response]
 	
 	# Might want to catch a failure?
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
 	if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
 		set memoq_doc_guids ""
 	} else {
	 	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	 	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
	 	set success [CkXml_FindChild2 $responseXml "DocumentGuids"]
	 	
	 	# It might return multiple guids
	 	set success [CkXml_FirstChild2 $responseXml]
	 	set memoq_doc_guids [list]
	 	while {[expr $success == 1]} {
	 		lappend memoq_doc_guids [CkXml_content $responseXml]"
	 		set success [CkXml_NextSibling2 $responseXml]
	 	}
 	}
 	delete_CkXml $responseXml
 	return $memoq_doc_guids
}
