# /packages/intranet-trans-memoq/tcl/intranet-trans-memoq-procs.tcl

ad_library {
   MemoQ server applications

	@author malte.sussdorff@cognovis.de
}
package require base64
namespace eval intranet_memoq::TMService {}

ad_proc -public intranet_memoq::TMService::GetTMInfo {
	-tmGuid
	-array_name
} {
	Returns an array of the queried TM
	
	@param tmGuid	GUID for the Translation Memory
	@param array_name Name of the array to write into
} {
	upvar 1 $array_name tm_array
	if {[info exists tm_array]} {
			unset tm_array
	}
	
	set operation "GetTMInfo"
	set m:GetTMInfo(m:tmGuid) $tmGuid
	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:GetTMInfo"
	
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]

	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]
		
	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
	
	set success [CkXml_FirstChild2 $responseXml]
	while {[expr $success == 1]} {
		ns_log Notice "[CkXml_tag $responseXml] : [CkXml_content $responseXml]"
		set success [CkXml_NextSibling2 $responseXml]
	}
	
	delete_CkXml $responseXml

}


ad_proc -public intranet_memoq::TMService::ListTMs {
	{-srcLang ""}
	{-targetLang ""}
	-ignore_project_tms:boolean
} {
	Returns a list of TMs

	@param srcLang 		Source Language in MemoQ (e.g. ger/eng)
	@param targetLang	Target Language in MemoQ
	@return List of TMs with the TM info a list Guid Name Client Domain Subject Project SourceLanguageCode TargetLanguageCode
} {
#	set tmXml [new_CkXml]
	set operation "ListTMs"
	
	if {$srcLang ne ""} {
		set m:ListTMs(m:srcLang) "$srcLang"
	}
	
	if {$targetLang ne ""} {
	 	set m:ListTMs(m:targetLang) "$targetLang"
	}
	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:ListTMs"
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]
	set responseXml [new_CkXml]
	set success [CkXml_LoadXml $responseXml $response]

	set success [CkXml_FindChild2 $responseXml "s:Body"]
	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
	set tms_list [intranet_memoq::ResultListOfLists -responseXml $responseXml -Tag "TMInfo"]
	
	delete_CkXml $responseXml
	return $tms_list
}

ad_proc -public intranet_memoq::TMService::TMInfo {
	-source_language_code
	-target_language_code
	-company_id
	{-subject ""}
	{-name ""}
	{-description ""}
	-xml
} {
	Creates a TM info element
} {
		set childNode [CkXml_NewChild $xml "m:info" ""]
		
		# ResourceInfo Class
		# Base class for resource information objects (such as the TMInfo
		# and TBInfo classes encapsulating common information for a TM/TB, such as Guid, name and description).
		
		CkXml_NewChild $childNode m:Description $description
		CkXml_NewChild $childNode m:Name $name
		CkXml_NewChild $childNode m:Readonly "false"
		
		# HeavyResourceInfo
		# Base class for heavy resources (TMInfo and TBInfo classes) including some meta data (Domain, Subject, etc.).
		CkXml_NewChild $childNode m:Client [db_string company_path "select company_path from im_companies where company_id = :company_id" -default ""]
		CkXml_NewChild $childNode m:Domain ""
		CkXml_NewChild $childNode m:Project ""
		CkXml_NewChild $childNode m:Subject $subject
		
		# TMInfo
		# Encapsulates the description of a TM. This class is derived from ResourceInfo.
		CkXml_NewChild $childNode m:AllowMultiple "false"
		CkXml_NewChild $childNode m:AllowReverseLookup "true"
		CkXml_NewChild $childNode m:SourceLanguageCode "$source_language_code"
		CkXml_NewChild $childNode m:StoreFormatting "true"
		CkXml_NewChild $childNode m:TargetLanguageCode "$target_language_code"
		CkXml_NewChild $childNode m:UseContext "true"
		CkXml_NewChild $childNode m:UseIceSpriceContext "false"
		return $childNode
}

ad_proc -public intranet_memoq::TMService::CreateAndPublish {
	-source_language_id
	-target_language_id
	-company_id
	{-subject_area_id ""}
	{-name ""}
	{-description ""}
	
} {
	Creates and publishes a new TM
	
	@return List of GUID and name for the new TM
} {
	set operation "CreateAndPublish"
		
	set source_language_code [im_category_from_id -translate_p 0 $source_language_id]
	set target_language_code [im_category_from_id -translate_p 0 $target_language_id]
	set company_name [im_name_from_id $company_id]
	
	if {$name eq ""} {
		set name "${company_name}_${source_language_code}_$target_language_code"
	}
	
	if {$subject_area_id ne ""} {
		set subject [im_category_from_id $subject_area_id]
		append name "_$subject"
	} else {
		set subject ""
	}

	# Check if the name already exists...
	
	set tm_lists [intranet_memoq::TMService::ListTMs -srcLang $source_language_code -targetLang $target_language_code]
	set tm_guid ""
	foreach tm_list $tm_lists {
		set tm_name [lindex $tm_list 1]
		if {$name eq $tm_name} {
			set tm_guid [lindex $tm_list 0]
		}
	}
	
	if {$tm_guid eq ""} {
		set newXml [new_CkXml]
		CkXml_put_Tag $newXml "m:CreateAndPublish"

		set childNode [intranet_memoq::TMService::TMInfo \
			-xml $newXml \
			-source_language_code $source_language_code \
			-target_language_code $target_language_code \
			-company_id $company_id \
			-subject $subject \
			-name $name \
			-description $description
		]
		CkXml_GetRoot2 $newXml
	
		set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $newXml]
		set responseXml [new_CkXml]
		set success [CkXml_LoadXml $responseXml $response]
		set success [CkXml_FindChild2 $responseXml "s:Body"]
		
		if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
			ns_log Error "[CkXml_getXml $responseXml]"
			set tm_guid 0
		} else {
			set success [CkXml_FindChild2 $responseXml "${operation}Response"]
			set success [CkXml_FindChild2 $responseXml "${operation}Result"]
			set tm_guid [CkXml_content $responseXml]
		}
		delete_CkXml $childNode
		delete_CkXml $responseXml
	}
	return [list $tm_guid $name]
}

ad_proc -public intranet_memoq::TMService::BeginChunkedTMXImport {
	-tmGuid
} {
	Starts a new chunked TMX import session.
	
	@param tmGuid The guid of an already existing TM into which the TMX data is to be imported.
	@return The session id (guid) of the newly started chunked TMX import session. It should be provided as first parameter for the AddNextTMXChunk and EndChunkedTMXImport operations.
} {
	set operation "BeginChunkedTMXImport"
	set m:BeginChunkedTMXImport(m:tmGuid) $tmGuid
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]
 	set success [CkXml_LoadXml $responseXml $response]
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
 	set session_id [CkXml_content $responseXml]
	delete_CkXml $responseXml
	
	return $session_id
}

ad_proc -public intranet_memoq::TMService::AddNextTMXChunk {
	-sessionId
	-tmxData
} {
	Performs the import of a TMX file chunk. The operation should be called in turns to import the next chunk of the TMX document. It is not required that the chunk ends/begins at a valid TMX XML part. The operation does not return until the segments included in the TMX chunk are imported (except from the last partially provided segment of the chunk). It is important that the interval between two AddNextTMXChunk calls (and the interval between the call of BeginChunkedTMXImport and the first call of AddNextTMXChunk) is less than a minute or two. If the interval is larger, then the TMX import session times out on the server and reserved resources (such as TM locks) are released, the import can not continue.
	
	@param sessionId The session id (guid) of the chunked TMX import session created by BeginChunkedTMXImport.
	@param tmxData The next chunk of data to be imported into the TM. The data size should be approximately 500 Kbytes (better estimation may be required, approximately 10000 segments should be included). The encoding of the chunk must be UTF-16 LE.
	@return 1 if successful
} {
	set operation "AddNextTMXChunk"
	set m:AddNextTMXChunk(m:sessionId) $sessionId
	set m:AddNextTMXChunk(m:tmxData) $tmxData
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]
 	set success [CkXml_LoadXml $responseXml $response]
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
 
 	# Might want to catch a failure?
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
	if {[CkXml_FindChild2 $responseXml "s:Fault"]} {
		set success_p 0
	} else {
		set success_p 0
	}
	delete_CkXml $responseXml
	return $success_p
}


ad_proc -public intranet_memoq::TMService::EndChunkedTMXImport {
	-sessionId
} {
	Call to indicate to the MemoQ Server that all chunks have been sent by calling AddNextTMXChunk. Closes the chunked TMX import session. It is very important to call this method as soon as possible after importing the last chunk of data to release server resources (such as TM locks).
	
	@param sessionId The session id (guid) of the chunked TMX import session to be closed. Always provide an id cerated by the BeginChunkedTMXImport operation.
	@return Returns number of imported segments
} {
	set operation "EndChunkedTMXImport"
	set m:EndChunkedTMXImport(m:sessionId) $sessionId
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]

 	CkXml_LoadXml $responseXml $response
 	
	CkXml_FindChild2 $responseXml "s:Body"
 	CkXml_FindChild2 $responseXml "${operation}Response"
 	CkXml_FindChild2 $responseXml "${operation}Result"
	CkXml_FindChild2 $responseXml "ImportedSegmentCount"
	
	set imported_segments [CkXml_content $responseXml]
	delete_CkXml $responseXml

	return $imported_segments
}

ad_proc -public intranet_memoq::TMService::BeginChunkedTMXExport {
	-tmGuid
} {
	Starts a new chunked TMX Export session.

	@param tmGuid The guid of an already existing TM into which the TMX data is to be Exported.
	@return The session id (guid) of the newly started chunked TMX Export session. It should be provided as first parameter for the AddNextTMXChunk and EndChunkedTMXExport operations.
} {
	set operation "BeginChunkedTMXExport"
	set m:BeginChunkedTMXExport(m:tmGuid) $tmGuid
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]
 	set success [CkXml_LoadXml $responseXml $response]
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
 	set session_id [CkXml_content $responseXml]
	delete_CkXml $responseXml

	return $session_id
}

ad_proc -public intranet_memoq::TMService::GetNextTMXChunk {
	-sessionId
} {
	Performs the export of a TMX document. The operation should be called in turns to get the next chunk of the TMX document. It is important that the time interval between two GetNextTMXChunk calls (and the interval between the call of BeginChunkedTMXExport and the first call of GetNextTMXChunk) is less than a minute or two. If the interval is larger, the TMX export session times out on the server and reserved resourced (such as TM locks) are released, /// the export can not continue. It is not guaranteed that the chunk ends/begins at valid TMX XML part.

	@param sessionId The session id (guid) of the chunked TMX import session created by BeginChunkedTMXImport.
	@return The next chunk of TMX data exported from the TM. If no more data is available, the operation returns null, or an empty array of bytes.
} {
	set operation "GetNextTMXChunk"
	set m:GetNextTMXChunk(m:sessionId) $sessionId
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]
 	set success [CkXml_LoadXml $responseXml $response]
 	set success [CkXml_FindChild2 $responseXml "s:Body"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Response"]
 	set success [CkXml_FindChild2 $responseXml "${operation}Result"]
 	set chunk [CkXml_content $responseXml]
	delete_CkXml $responseXml
	return $chunk
}


ad_proc -public intranet_memoq::TMService::EndChunkedTMXExport {
	-sessionId
} {
	Call to indicate to the MemoQ Server that all chunks have been sent by calling AddNextTMXChunk. Closes the chunked TMX import session. It is very important to call this method as soon as possible after importing the last chunk of data to release server resources (such as TM locks).

	@param sessionId The session id (guid) of the chunked TMX import session to be closed. Always provide an id cerated by the BeginChunkedTMXImport operation.
	@return Returns number of imported segments
} {
	set operation "EndChunkedTMXExport"
	set m:EndChunkedTMXExport(m:sessionId) $sessionId
 	intranet_chilkat::array_to_xml -xml_name tmXml -array_name "m:$operation"
	set response [im_trans_memoq_call -service_name TMService -operation $operation -m_Xml $tmXml]
 	set responseXml [new_CkXml]

 	CkXml_LoadXml $responseXml $response

	CkXml_FindChild2 $responseXml "s:Body"
 	CkXml_FindChild2 $responseXml "${operation}Response"
 	CkXml_FindChild2 $responseXml "${operation}Result"
	CkXml_FindChild2 $responseXml "ImportedSegmentCount"

	set imported_segments [CkXml_content $responseXml]
	delete_CkXml $responseXml

	return $imported_segments
}