# packages/intranet-trans-memoq/tcl/intranet-trans-memoq-callback-procs.tcl

## Copyright (c) 2017, cognovís GmbH, Hamburg, Germany
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see
# <http://www.gnu.org/licenses/>.
#

ad_library {
    
    MemoQ Procs
    
    @author  malte.sussdorff@cognovis.de
    @creation-date 2017-07-14
    @cvs-id $Id$
}


# ---------------------------------------------------------------
# PROJECT callbacks
# ---------------------------------------------------------------

ad_proc -public -callback im_project_after_create -impl memoq_create_project {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Create the Project in MemoQ and store the result in project-open
} {
    if {[im_trans_memoq_configured_p]} {
	ns_log Debug "Running memoq project creation"
	if {[im_category_is_a $type_id [im_project_type_translation]]} {
	    set result [im_trans_memoq_create_project -project_id $object_id]
	    sencha_errors::display_errors_and_warnings -object_id $object_id
	}
    }
}

ad_proc -public -callback im_project_after_file_upload -impl memoq_after_file_upload {
    {-project_id:required}
    {-status_id ""}
    {-type_id ""}
} {
	after file upload callback
} {
    im_trans_memoq_file_upload_source_files -project_id $project_id
    im_trans_memoq_project_analysis -project_id $project_id
    sencha_errors::display_errors_and_warnings -object_id $project_id
}

ad_proc -public -callback im_project_after_update -impl memoq_update_project {
	{-object_id:required}
	{-status_id ""}
	{-type_id ""}
} {
	Create the Project in MemoQ and store the result in ]project-open[
} {
    if {[im_category_is_a $type_id [im_project_type_translation]]} {
	set project_id $object_id
	db_1row project_info "select project_nr, company_path, description, to_char(end_date,'YYYY-MM-DD') || 'T' || to_char(end_date,'HH24:MI:SS') as deadline, im_category_from_id(subject_area_id) as subject, memoq_guid from im_projects p where project_id = :project_id"
	
	set domain [memoq_project_domain -project_id $project_id]
	intranet_memoq::ServerProjectService::UpdateProject \
	    -ServerProjectGuid $memoq_guid \
	    -Description $description \
	    -Deadline $deadline \
	    -Domain $domain \
	    -Subject $subject \
	    -Client $company_path \
	    -Project $project_nr
    }
}

ad_proc -public -callback im_project_after_update -impl memoq_update_project {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Update the project (especially the name) in memoq.
} {
    if {[im_category_is_a $type_id [im_project_type_translation]]} {
	set project_id $object_id
	
	db_1row project_info "select project_name, project_nr, description, end_date from im_projects p where project_id = :project_id"
	# And here goes the update code
    }
}

ad_proc -public -callback im_project_after_update -impl memoq_wrapup {
    {-object_id:required}
    {-status_id ""}
    {-type_id ""}
} {
    Finalize the Project in MemoQ when the Projekt is closed in ]project-open[
} {
    set closed_ids [im_sub_categories 81]
    if {[im_category_is_a $type_id [im_project_type_translation]] && [lsearch $closed_ids $status_id]>-1} {
	set project_id $object_id
	db_1row project_info "select project_name, project_nr, description, end_date from im_projects p where project_id = :project_id"
	
	# And here goes the wrapup code
    }
}

ad_proc -public -callback intranet-translation::project_info -impl memoq_links {
    {-project_id:required}
} {
    Append the links to memoq for project_info
} {
    if {[im_trans_memoq_configured_p]} {
	upvar project_info project_info
	db_1row project_info "select memoq_guid
    from im_projects
    where project_id = :project_id"
	
	if {$memoq_guid ne ""} {
	    set memoq_server [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQServer"]
	    set memoq_port [parameter::get -package_id [im_package_trans_memoq_id] -parameter "MemoQPort"]
	    set memoq_project_url [export_vars -base "${memoq_server}:${memoq_port}/memoq/pm/projectoverview/index/$memoq_guid" -url {{nocache True}}]
	    template::multirow append project_info "MemoQ Project" "" "MemoQ" "" "" "" "<a href='$memoq_project_url'>Open Project</a>"
	}
    }
}
