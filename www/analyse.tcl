ad_page_contract {
    page to create the project and analyse it
    
    @author malte.sussdorff@cognovis.de
    @creation-date 2017-07-20
    @cvs-id $Id$
} {
    project_id:integer,notnull
}

if {[db_string memoq "select memoq_guid from im_projects where project_id = :project_id" -default ""] eq ""} {
    set memoq_guid [im_trans_memoq_create_project -project_id $project_id]
}

im_trans_memoq_project_analysis -project_id $project_id

ad_returnredirect [export_vars -base "/intranet-translation/trans-tasks/task-list" -url {project_id}]
