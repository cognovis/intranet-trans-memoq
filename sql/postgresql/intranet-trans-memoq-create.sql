SELECT acs_log__debug('/packages/intranet-trans-memoq/sql/postgresql/upgrade/upgrade-4.1.0.0.0-4.1.0.0.1.sql','');

alter table im_projects add column memoq_guid text;
alter table im_trans_tasks add column memoq_doc_guid text;

create table im_trans_memoq_resources (
	guid				text primary key,
	type				text,
	source_language_id integer not null
						constraint im_trans_memoq_source_fk
						references im_categories,
	target_language_id integer not null
						constraint im_trans_memoq_target_fk
						references im_categories,
	company_id 			integer not null
						constraint im_trans_memoq_company_fk
						references im_companies,
	subject_area_id 	integer
						constraint im_trans_memoq_subject_fk
						references im_categories,
	name				text,
	description			text,
	filename			text,
	upload_date			timestamp,
	segments			integer
);


SELECT im_component_plugin__new (
				null,                           -- plugin_id
				'acs_object',                   -- object_type
				now(),                          -- creation_date
				null,                           -- creation_user
				null,                           -- creation_ip
				null,                           -- context_id
				'Intranet Company MemoQ Resources Component',        -- plugin_name
				'intranet-trans-memoq',                  -- package_name
				'right',                        -- location
				'/intranet/companies/view',      -- page_url
				null,                           -- view_name
				10,                             -- sort_order
				'im_trans_memoq_resources_component -user_id $user_id -company_id $company_id -return_url $return_url',
		'lang::message::lookup "" intranet-trans-memoq.Company_Resources_List "Company Resources"'
);

-- Make the component readable for employees and poadmins
CREATE OR REPLACE FUNCTION inline_0 ()
RETURNS integer AS '
DECLARE

		v_object_id     integer;
		v_employees     integer;
		v_poadmins      integer;

BEGIN
		SELECT group_id INTO v_poadmins FROM groups where group_name = ''P/O Admins'';

		SELECT group_id INTO v_employees FROM groups where group_name = ''Employees'';

		SELECT plugin_id INTO v_object_id FROM im_component_plugins WHERE plugin_name = ''Intranet Company MemoQ Resources Component'';

		PERFORM im_grant_permission(v_object_id,v_employees,''read'');
		PERFORM im_grant_permission(v_object_id,v_poadmins,''read'');


		RETURN 0;

END;' language 'plpgsql';

SELECT inline_0 ();
DROP FUNCTION inline_0 ();

insert into cr_mime_types (label,mime_type,file_extension) values ('MemoQ Translation Memory','application/memoq_tm','tmx');
insert into cr_extension_mime_type_map (mime_type,extension) values ('application/memoq_tm','tmx');
