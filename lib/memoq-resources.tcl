ad_page_contract {
	page to view resources

	@author malte.sussdorff@cognovis.de
	@creation-date 2017-07-20
	@cvs-id $Id$
} -query {
	{orderby:optional}
} -properties {
	company_id:onevalue
	return_url:onevalue
}

# ---------------------------------------------------------------
# prepare list of files
# ---------------------------------------------------------------

set actions [list]

# for now, invite users to upload, and then they will be asked to
# login if they are not.

set cancel_url "[ad_conn url]?[ad_conn query]"

set elements {
	type {
		label {[_ intranet-core.Type]}
	}
	name {
		label {[_ intranet-core.Name]}
	}
	source_language {
		label {[_ intranet-translation.Source_Language]}
	}
	target_language {
		label {[_ intranet-translation.Target_Language]}
	}
	subject_area {
		label {[_ intranet-translation.Subject_Area]}
	}
	filename {
		label {[_ file-storage.Name]} \
	}
	segments {
		label {[_ intranet-trans-memoq.Segments]}
	}
	upload_date {
		label {[_ intranet-translation.Modified_Date]}
	}
}

template::multirow create contents type guid name source_language target_language subject_area filename segments upload_date

set sql "select guid, type, im_name_from_id(source_language_id) as source_language,
	im_name_from_id(target_language_id) as target_language, im_name_from_id(subject_area_id) as subject_area,
	name, filename, segments,upload_date from im_trans_memoq_resources where company_id = :company_id"
db_foreach resources $sql {
	
	set upload_date [lc_time_fmt $upload_date "%x %X"]
	template::multirow append contents $type $guid $name $source_language $target_language $subject_area $filename $segments $upload_date
}

lappend actions "#file-storage.Add_File#" [export_vars -base "/intranet-trans-memoq/resource-upload" {company_id return_url}] "[_ file-storage.lt_Upload_a_file_in_this]"

# set bulk_actions [list [_ intranet-core.Delete] "/intranet-trans-memoq/resource-delete" "Delete selected Resource"]
set bulk_actions [list]
template::list::create \
	-name contents \
	-multirow contents \
	-key guid \
	-actions $actions \
	-elements $elements \
	-bulk_actions $bulk_actions \
	-bulk_action_export_vars { company_id return_url } \
	-bulk_action_method post


ad_return_template
